<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function multiplier(Request $request)
    {
        $nb1 = $request->nb1;
        $nb2 = $request->nb2;
        $res = $nb1*$nb2;
        return view('res', ['res' => $res]);
    }
}
